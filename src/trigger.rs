use serde_json::Value;

#[derive(Debug, Deserialize, Serialize)]
pub struct Trigger {
    name: String,
    data: Value,
}

impl Trigger {
    pub fn new<T: Into<String>>(name: T) -> Trigger {
        Trigger {
            name: name.into(),
            data: Value::Null
        }
    }

    pub fn data(mut self, data: &Value) -> Self {
        self.data = data.clone();
        self
    }
}